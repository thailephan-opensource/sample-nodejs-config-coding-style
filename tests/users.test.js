const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../index');

const expect = chai.expect;

chai.should();
// Or should when object create with Object.create(null)
// eslint-disable-next-line no-unused-vars
const should = chai.should();

chai.use(chaiHttp);

// BDD
describe('Users', () => {
    // it()
    // describe()
    // before()
    // beforeEach()
    // after()
    // afterEach()

    describe('Get all users', () => {
        it('It should get all users', (done) => {
            chai.request(server)
                .get('/users')
                .end((err, res) => {
                    expect(res.status).to.equal(200);

                    // Same each other
                    expect(res.body.data).to.be.an('array');
                    res.body.data.should.be.an('array');

                    expect(res.body.data.length).to.be.equal(3);
                    done();
                });
        });
    });

    describe('Get one users', () => {
        it('should get first users', (done) => {
            const USER_ID = 0;
            chai.request(server)
                .get(`/users/${USER_ID}`)
                .end((err, res) => {
                    expect(res.status).to.equal(200);
                    expect(res.body.data).to.be.an('object');
                    expect(res.body.data).to.have.property('name', 'Peter');

                    done();
                });
        });
    });

    describe('Post one users', () => {
        it('Response new user from user data', (done) => {
            chai.request(server)
                .post('/users')
                .send({name: 'Xayah'})
                .end((err, res) => {
                    expect(res.status).to.equal(200);
                    expect(res.body.data).to.be.an('object');
                    expect(res.body.data).to.have.property('name', 'Xayah');

                    done();
                });
        });
    });
});
