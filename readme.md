# Sample nodejs config coding style
[![pipeline status](https://gitlab.com/thailephan-opensource/sample-nodejs-config-coding-style/badges/master/pipeline.svg)](https://gitlab.com/thailephan-opensource/sample-nodejs-config-coding-style/-/commits/master)
[![coverage report](https://gitlab.com/thailephan-opensource/sample-nodejs-config-coding-style/badges/master/coverage.svg)](https://gitlab.com/thailephan-opensource/sample-nodejs-config-coding-style/-/commits/master)
[![Latest Release](https://gitlab.com/thailephan-opensource/sample-nodejs-config-coding-style/-/badges/release.svg)](https://gitlab.com/thailephan-opensource/sample-nodejs-config-coding-style/-/releases)


[Refactor implements](https://viblo.asia/p/nang-cao-chat-luong-code-va-hieu-qua-lam-viec-nhom-voi-husky-lint-staged-commitlint-4dbZNnMnZYM#_cai-thien-chat-luong-code-voi-eslint-va-prettier-2)
# Local check
## Eslint
[**Eslint configuration for Typescript**](https://viblo.asia/p/cau-hinh-eslint-cho-du-an-typescript-XL6lA9pAlek)

**Eslint configuration for Javascript**
1. Install `eslint` with google eslint standard
```console
$ yarn add -D eslint eslint-config-google
```
2. Add to `package.json`
```jsonc
  "scripts": {
    "postinstall": "husky install", // run husky install after install packages
    ...
    "lint": "eslint \"./**/*.js\" --fix", // eslint check
    "format": "prettier --write \"./**/*.js\"" // prettier format
  },
```

3. Add `.eslintrc.json` and `.eslintignore`

*.eslintrc.json*
```jsonc
{
    "env": {
        "commonjs": true,
        "es2021": true,
        "node": true,
        "jest": true
    },
    "extends": ["eslint:recommended", "google"],
    "parserOptions": {
        "ecmaVersion": 12
    },
    "rules": {
        "semi": 2,
        "new-cap": 0,
        "indent": [1, 4]
    }
}
```
*.eslintignore*
```
node_modules
.docker
coverage
jest.config.js
```
4. (Optional) Mannual execute eslint
```console
$ npm run lint
```
5. (Optional) Add code actoin on save to auto fix eslint error
- Open setting.json file
- Paste below code into file

```jsonc
  "editor.codeActionsOnSave": {
    "source.fixAll.tslint": true,
    "source.fixAll.eslint": true,
  }
```
## Prettier
1. Install `prettier`
```
yarn add -D prettier
```
2. Add to `package.json`
```json
  "scripts": {
    "postinstall": "husky install", // run husky install after install packages
    ...
    "lint": "eslint \"./**/*.js\" --fix", // eslint check
    "format": "prettier --write \"./**/*.js\"" // prettier format
  },
```
3. Add `.prettierrc` and `.prettierignore`

*.prettierrc*
```json
{
  "singleQuote": true,
  "bracketSpacing": false,
  "printWidth": 80,
  "jsxSingleQuote": true,
  "arrowParens": "always",
  "trailingComma": "all",
  "useTabs": false,
  "tabWidth": 4,
  "endOfLine": "lf"
}
```
*.prettierignore*
```
node_modules
.docker
coverage
```
4. (Optional) Mannual execute prettier
```
npm run format
```
## Lint-staged
Lint-staged cho phép ta thực hiện một hoặc một số công việc chỉ với những file được git staged, hiểu đơn giản là những file vừa được thêm vào hoặc có sự thay đổi ở thời điểm hiện tại. Do đó thay vì format cả 100 file, ta chỉ cần format 1 file mà ta vừa sửa, giúp ta tiết kiệm thời gian.
1. Install `lint-staged`
```
npm install --save-dev lint-staged
```
2. Add lines below to `package.json` or add more command if need into array
```json
  "lint-staged": {
    "*.js": [ // This is for javascript files
      "npm run lint", 
      "npm run format",
      "git add ."
    ],
    "*.ts": [ // This is for typescript files
      "npm run lint",
      "npm run format",
      "git add ."
    ]
  }
```
3. Add new `lint:staged` script to `package.json`
```json
"scripts": {
  "postinstall": "husky install",
  ...
  "lint": "eslint \"./**/*.js\" --fix",
  "lint:staged": "lint-staged",
  "format": "prettier --write \"./**/*.js\""
}
```
## Husky
Husky là một tool mà nó có thể bắt được event khi ta thao tác với Git repository (add, commit,...) và từ đó ta có thể thực hiện các hành động tương ứng, hoặc ngăn không cho commit. Husky và Lint-staged là 1 cặp bài trùng thường đi cùng với nhau, cực kì hữu dụng trong việc đảm bảo code khi được commit lên repository luôn được chạy qua những công đoạn kiểm tra để chắc chắn "code sạch, code đẹp"
1. Install `husky`
```console
$ npm install --save-dev husky
```
2. Init `husky` configuration
```console
$ npx husky install
```
3. Run script to husky to catch `pre-commit` event (before user commit)
```console
$ npx husky add .husky/pre-commit "yarn lint-staged"
```
4. Add new `lint:staged` script to `package.json`
```json
"scripts": {
  "postinstall": "husky install",
  ...
  "lint": "eslint \"./**/*.js\" --fix",
  "lint:staged": "lint-staged",
  "format": "prettier --write \"./**/*.js\""
}
```
**Note**: Developers can skip husky check by commit with `--no-verify` option
```console
$ git commit -m <message> --no-verify
```
## Commitlint format
CommitLint giúp đảm bảo được tất cả các commit đều phải có nội dung theo chuẩn
```
type(scope?): subject
```
### Type
- build: Changes that affect the build system or external dependencies (example scopes: gulp, broccoli, npm)
- ci: Changes to our CI configuration files and scripts (example scopes: Gitlab CI, Circle, BrowserStack, SauceLabs)
- chore: add something without touching production code (Eg: update npm dependencies)
- docs: Documentation only changes
- feat: A new feature
- fix: A bug fix
- perf: A code change that improves performance
- refactor: A code change that neither fixes a bug nor adds a feature
- revert: Reverts a previous commit
- style: Changes that do not affect the meaning of the code (Eg: adding white-space, formatting, missing semi-colons, etc)
- test: Adding missing tests or correcting existing tests

### Scope (optional)
- Tên của package mà commit hiện tại làm ảnh hưởng. Mình thấy scope thường dùng ở các repository mà chứa nhiều packages dạng monorepo

### Subject
- Commit message

Example: `git commit -m "chore: add commit lint"`

1. Install `commitlint`
- Linux
```console
$ npm install --save-dev @commitlint/{config-conventional,cli}
```
- Window
```console
$ npm install --save-dev @commitlint/{config-conventional,cli}
```

2. Run script to husky to catch `commit-msg` event (take out commit message to verify)
```console
$ npx husky add .husky/commit-msg ""
```
3. Edit `.husky/commit-msg`
```
#!/bin/sh
. "$(dirname "$0")/_/husky.sh"

npx --no-install commitlint --edit "$1"
```
4. Add commit lint file `.commitlintrc.js`
```typescript
module.exports = {extends: ['@commitlint/config-conventional']};
```

# Remote check (CICD)