require('dotenv').config();

const express = require('express');
const app = express();

const usersRouter = require('./routers/users.router');

app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.use('/users', usersRouter);

const PORT = process.env.PORT || 5000;

app.listen(PORT, () => {
    console.log(`Server is running at port ${PORT}`);
});

module.exports = app;
