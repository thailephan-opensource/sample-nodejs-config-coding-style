const express = require('express');

const usersRouter = express.Router();

const users = [{name: 'Peter'}, {name: 'Anychan'}, {name: 'Kaya'}];

usersRouter.get('/:id', (req, res) => {
    return res.json({
        data: users[req.params.id],
    });
});

usersRouter.get('/', (req, res) => {
    return res.json({
        data: users,
    });
});

usersRouter.post('/', (req, res) => {
    return res.json({
        data: req.body,
    });
});

module.exports = usersRouter;
