FROM node:16.16-alpine
WORKDIR /app

# RUN apk update &&  rm -rf /var/cache/apk/*

RUN npm install --location=global pm2 && pm2 install pm2-logrotate

COPY ["package*.json", "./"]

# Remove if don't want to install devDependencies'
RUN npm ci --slient --omit=dev && npm cache clean --force

COPY . .

# CMD ["pm2", "start", "ecosystem.config.js"]
CMD ["pm2", "start", "index.js","--name","my-project","--no-daemon", "--log-date-format='MM-DD hh:mm:ss.SSS A'"]